<?php  
	header('content-type: image/jpeg');  
	
	//watermark source image
	$watermark = imagecreatefrompng('watermark.png'); 
	$watermark_width = imagesx($watermark); 
	$watermark_height = imagesy($watermark);  
	$image = imagecreatetruecolor($watermark_width, $watermark_height);  
	
	//image to add watermark to
	$image = imagecreatefromjpeg($_GET['src']);
	$size = getimagesize($_GET['src']);  
	$dest_x = $size[0] - $watermark_width - 5; 
	$dest_y = $size[1] - $watermark_height - 5;  
	imagecopymerge($image, $watermark, $dest_x, $dest_y, 0, 0, $watermark_width, $watermark_height, 100);  
	imagejpeg($image);
	imagedestroy($image); 
	imagedestroy($watermark);
	
?>  